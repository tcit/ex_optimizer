# Changelog

## 0.1.1
2020-10-24

### Changed

* Move optimizers binary name to module attributes
* Update dependencies
* Update CI Dockerfile

### Fixed

* Remove usage of depreciated `Code.ensure_compiled?/1` function
* Fixed Credo warnings

## 0.1.0
2019-10-20

* Initial release
